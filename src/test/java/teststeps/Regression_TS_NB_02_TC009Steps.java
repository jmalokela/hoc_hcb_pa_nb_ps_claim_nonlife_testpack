package teststeps;

import commonFunctions.CommonFunctions;
import context.TestContext;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pageobjects.ApplicationDataCapturePage;
import pageobjects.BusinessOperationsPage;
import pageobjects.SecureLandingPage;
import reporting.ExtentReportConfig;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Regression_TS_NB_02_TC009Steps {

    private TestContext testContext;
    private SecureLandingPage secureLandingPage;
    private BusinessOperationsPage businessOperationsPage;
    private ApplicationDataCapturePage applicationDataCapturePage;
    public CommonFunctions commonFunctions;
    public LoginSteps loginSteps;
    private Robot r;

    public Regression_TS_NB_02_TC009Steps(TestContext context) throws Exception {
        secureLandingPage = context.getPageObjectManager().getSecureLandingPage();
        commonFunctions = new CommonFunctions(context);
        businessOperationsPage = context.getPageObjectManager().getBusinessOperationsPage();
        applicationDataCapturePage = context.getPageObjectManager().getApplicationDateCapturePage();
        r = new Robot();
        ExtentReportConfig.childTest = ExtentReportConfig.extent.startTest("To validate the policy issuance of HOC and payment cycle generation");
    }

    @Given("^user is logged in$")
    public void user_is_logged_in() throws Exception {
        commonFunctions.Login("NB316365","kolkata@1","HO","OP-HEAD");
    }

    @Then("^click on business operations$")
    public void click_on_business_operations() throws Exception {
        secureLandingPage.clickOperations();

    }

    @Then("^click on new business tab$")
    public void click_on_new_business_tab() throws Exception {
        businessOperationsPage.clickNewBusiness();

    }

    @Then("^click on Application Data Capture$")
    public void click_on_Application_Data_Capture()  {
        businessOperationsPage.clickApplicationDataCapture();

    }

    @Then("^select LOB and product as homeowner cover$")
    public void select_LOB_and_product_as_homeowner_cover() throws Exception {
        businessOperationsPage.selectLOB("PROPERTY AND CASUALITY");
        businessOperationsPage.selectProduct("HOME OWNERS COVER");

    }

    @Then("^click on proceed button$")
    public void click_on_proceed_button() throws Exception {
        businessOperationsPage.clickProceed();
    }

    @Then("^enter expiry date$")
    public void enter_expiry_date() throws Exception {
        businessOperationsPage.enterExpiryDate("05052025");
        r.keyPress(KeyEvent.VK_TAB);
        r.keyRelease(KeyEvent.VK_TAB);
    }

    @Then("^search for party holder by  opening party search form$")
    public void search_for_party_holder_by_opening_party_search_form() throws Exception {
        businessOperationsPage.clickPartyHolderLookup();
    }

    @Then("^enter the Party Code$")
    public void enter_the_Party_Code() throws Exception {
        businessOperationsPage.enterPartyCode("PI00562403");
    }

    @Then("^click on Search button$")
    public void click_on_Search_button() throws Exception {
        businessOperationsPage.clickSearchButton();
    }

    @Then("^verify that the party holder details are returned$")
    public void verify_that_the_party_holder_details_are_returned(){
        businessOperationsPage.verifyPartyHolderDetailsReturned();
    }

    @Then("^select the party holder by clicking on the party code$")
    public void select_the_party_holder_by_clicking_on_the_party_code() throws Exception {
        businessOperationsPage.selectPartyHolder("PI00562403");
        Thread.sleep(1000);
    }

    @Then("^click on Save button and continue$")
    public void click_on_Save_button() throws Exception {

        businessOperationsPage.clickSaveButton();
    }

    @Then("^verify account details and click on continue$")
    public void verify_account_details_and_click_on_continue() throws Throwable {
        businessOperationsPage.clickContinueButton();
    }

    @Then("^select policy type$")
    public void select_policy_type() throws Throwable {
        applicationDataCapturePage.selectPolicyType("Internal");
    }

    @Then("^enter bond start date$")
    public void enter_bond_start_date() throws Throwable {
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String currentdate = dateFormat.format(new Date());
        applicationDataCapturePage.EnterPolicyDate(currentdate);
        r.keyPress(KeyEvent.VK_TAB);
        r.keyRelease(KeyEvent.VK_TAB);
    }

    @Then("^enter bond number$")
    public void enter_bond_number() throws Throwable {
        applicationDataCapturePage.enterBondNumber("0");
    }

    @Then("^select payment mode option$")
    public void select_payment_mode_option() throws Throwable {
        applicationDataCapturePage.selectPaymenrMode("Cash");
    }

    @Then("^click on save and continue button$")
    public void click_on_save_and_continue_button() throws Throwable {
        applicationDataCapturePage.clickSaveAndContinue();
    }


    @Then("^verify that the HOC Risk screen is displayed$")
    public void verifyThatTheHOCRiskScreenIsDisplayed() throws Exception {
        applicationDataCapturePage.verifyThatHOCRiskDetailsDisplayed();
    }

    @And("^click on Add button$")
    public void clickOnAddButton() throws Exception {
        applicationDataCapturePage.clickAddButton();
    }

    @And("^enter sum insured amount$")
    public void enterSumInsuredAmount() throws Exception {
        applicationDataCapturePage.enterSumInsuredAmount(800000);
    }

    @And("^enter address house number$")
    public void enterAddressHouseNumber() throws Exception {
        applicationDataCapturePage.enterHouseNUmber(1234);
    }

    @And("^enter address street name$")
    public void enterAddressStreetName() throws Exception {
        applicationDataCapturePage.enterStreetName("Rethabile");
    }

    @And("^enter suburb$")
    public void enterSuburb() throws Exception {
        applicationDataCapturePage.enterSuburb("COMMERCIA");
    }


    @And("^select the displayed suburb$")
    public void selectTheDisplayedSuburb() throws AWTException, InterruptedException {
        applicationDataCapturePage.selectSurburb("COMMERCIA");
    }


    @And("^enter the province$")
    public void enterTheProvince() throws Exception {

        applicationDataCapturePage.selectProvince("GAUTENG");
    }

    @And("^click on Save and Continue$")
    public void clickOnSaveAndContinue() {
    }
}
