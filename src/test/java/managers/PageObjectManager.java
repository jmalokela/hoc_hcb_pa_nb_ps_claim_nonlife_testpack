package managers;

import org.openqa.selenium.WebDriver;
import pageobjects.ApplicationDataCapturePage;
import pageobjects.BusinessOperationsPage;
import pageobjects.HomePage;
import pageobjects.SecureLandingPage;


public class PageObjectManager {
 
	private WebDriver driver;
	private HomePage homePage;
	private SecureLandingPage secureLandingPage;
	private BusinessOperationsPage businessOperationsPage;
	private ApplicationDataCapturePage applicationDataCapturePage;

	public PageObjectManager(WebDriver driver) {
 
		this.driver = driver;
	}

	public HomePage getHomePage(){
 
		return (homePage == null) ? homePage = new HomePage(driver) : homePage;
 
	}

	public SecureLandingPage getSecureLandingPage()
	{
		if(secureLandingPage == null)
		{
			secureLandingPage =  new SecureLandingPage(driver);
		}

		return  secureLandingPage;
	}

	public BusinessOperationsPage getBusinessOperationsPage()
	{
		if(businessOperationsPage == null)
		{
			businessOperationsPage =  new BusinessOperationsPage(driver);
		}

		return  businessOperationsPage;
	}

	public ApplicationDataCapturePage getApplicationDateCapturePage() throws Exception {
		if(applicationDataCapturePage ==null)
		{
			applicationDataCapturePage = new ApplicationDataCapturePage(driver);
		}
		else
		{
			throw new Exception("No Class found with name " + applicationDataCapturePage.getClass().getName());
		}
		return applicationDataCapturePage;
	}


}