package pageobjects;

import commonFunctions.ActionDriver;
import context.TestContext;
import org.apache.commons.codec.language.DoubleMetaphone;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.WebDriverEventListener;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ApplicationDataCapturePage {

    public WebDriver driver;
    private ActionDriver actionDriver;
    private String mainWindow;

    //Page Elements
    @FindBy(name="COND-T1Policy Type")
    private WebElement drpPolicyType;

    @FindBy(name="COND-T1Bond Start Date")
    private WebElement txtBondStartDate;

    @FindBy(name="COND-T1Bond Number")
    private WebElement txtBondNumber;

    @FindBy(name="COND-PROP2Secondary Bond Number Applicable")
    private WebElement drpSecondaryBondApplicable;

    @FindBy(name="COND-PROP4Add External Bond Account Number(s)")
    private WebElement drpAddExternalBondAccount;

    @FindBy(name="COND-T1Payment Frequency")
    private WebElement drpPaymentFrequency;

    @FindBy(name="COND-PAY1Payment Mode")
    private WebElement drpPaymentMode;

    @FindBy(name="COND-PAY3Any Sustained losses during previous 3 years")
    private WebElement drpAnySubsidiaryLosses;

    @FindBy(name="COND-PAY4Have You been refused insurance or had special terms imposed")
    private WebElement drpHaveYouRefusedInsurance;

    @FindBy(name="COND-PAY5Did you have a previous insurance policy")
    private WebElement drpDidYouPreviouslyHaveInsurance;

    @FindBy(name="COND-PAY6Commission Payable")
    private WebElement drpCommisionPayable;

    @FindBy(id="saveBtn")
    private WebElement btnSaveBtn;

    @FindBy(id="saveConBtn")
    private WebElement btnSaveandContinue;

    //Risk Details
    @FindBy(name="SEC-VHLLIST")
    private WebElement riskSectionSCreen;

    @FindBy(id="addBtn")
    private WebElement btnAdd;

    @FindBy(id="SEC-RSKTYPESum Insured")
    private WebElement txtSumInsured;

    @FindBy(id="SEC-LOCATIONHouse Number or ERF No-Address Line1")
    private WebElement txtHouseNumber;

    @FindBy(id="SEC-LOCATIONRoad (Number and Name)-Address Line2")
    private WebElement txtStreetName;

    @FindBy(id="SEC-LOCATIONSuburb")
    private WebElement txtSuburb;

    @FindBy(name="firstFocus")
    private WebElement btnSuburbElipses;

    @FindBy(xpath = "//*[text()='COMMERCIA']")
    private WebElement txtSelectSuburb;

    @FindBy(name="SEC-LOCATIONProvince")
    private WebElement drpProvince;

    @FindBy(name="saveBtn")
    private WebElement btnSaveAndContinue;

    public ApplicationDataCapturePage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
        actionDriver = new ActionDriver();
    }

    /**
     * Selecting the Policy Type
      * @param policyType
     * @throws Exception
     */
    public void selectPolicyType(String policyType) throws Exception {
        actionDriver.selectElementByVisibleText(drpPolicyType,policyType,"Policy Type field");
    }

    /**
     * Enter the Policy Start Date
     */
    public void EnterPolicyDate(String bondStartDate) throws Exception {
        actionDriver.enter(txtBondStartDate,bondStartDate,"Bond Start Date field");
    }

    /**
     * Entering Bond Number
     * @param bondNumber
     * @throws Exception
     */
    public void enterBondNumber(String bondNumber) throws Exception {
        actionDriver.enter(txtBondNumber,bondNumber,"Bond Number field");
    }

    /**
     * Selecting the commission payable option
     * @param commissionPayable
     * @throws Exception
     */
    public void selectCommisionPayableOption(String commissionPayable) throws Exception {
        actionDriver.selectElementByVisibleText(drpCommisionPayable,commissionPayable,"Commission Payable field");
    }

    /**
     * Selecting the Policy Payment Mode option
     * @param paymentmode
     * @throws Exception
     */
    public void selectPaymenrMode(String paymentmode) throws Exception {
        actionDriver.selectElementByVisibleText(drpPaymentMode,paymentmode,"Paymemt Mode dropdown field");
    }

    public void clickSaveAndContinue() throws Exception {
        actionDriver.click(btnSaveandContinue,"Save and Continue button");
    }

    public void verifyThatHOCRiskDetailsDisplayed() throws Exception {
        if(riskSectionSCreen.isDisplayed() && riskSectionSCreen !=null)
        {
            Assert.assertTrue("HOC Risk Details Screen Displayed",true);
        }
        else
        {
            throw new Exception("HOC Risk Details Screen Displayed");
        }
    }

    public void clickAddButton() throws Exception {
        mainWindow = driver.getWindowHandle();

        System.out.println("Main Window handling "+ mainWindow);

        actionDriver.click(btnAdd,"Add Button");

        for (String winHandle : driver.getWindowHandles()) {

            if(!mainWindow.equals(winHandle))
            {
                System.out.println("Second Window = :  " + winHandle);
                driver.switchTo().window(winHandle);
                break;
            }

        }

    }

    public void enterSumInsuredAmount(double suminsuredamount) throws Exception {
        String sumInsured = String.valueOf(suminsuredamount);
        actionDriver.enter(txtSumInsured,sumInsured,"Sum Insured Amount field");
    }

    public void enterHouseNUmber(int housenumber) throws Exception {
        String houseN = String.valueOf(housenumber);
        actionDriver.enter(txtHouseNumber,houseN,"House Number field");
    }

    public void enterStreetName(String streetname) throws Exception {
        actionDriver.enter(txtStreetName,streetname,"Street Name field");
    }

    public void enterSuburb(String suburb) throws Exception {

        System.out.println("Entering suburb...");
        actionDriver.enter(txtSuburb,suburb,"Suburb field");
        actionDriver.pressTab(1);
        actionDriver.pressEnter(1);

       /* Set <String> s = driver.getWindowHandles();
        Iterator <String> ite = s.iterator();
        int i = 1;
        while (ite.hasNext() && i < 10) {

            String popupHandle = ite.next().toString();
            System.out.println("Switched to second window " + popupHandle);
            driver.switchTo().window(popupHandle);

            System.out.println("Window title is :  " + i + driver.getTitle());

            if(!driver.getTitle().contains("TCS BαNCS Insurance"))
            {
                System.out.println("Window title is :  " + i + driver.getTitle());
                i++;
            }

            if (i == 3) {
                break;
            }
            i++;


        }*/
        //mainWindow = driver.getWindowHandle();
       // System.out.println("\nSecond Window on Suburb.... = " + driver.getWindowHandles());
        Thread.sleep(2000);
        actionDriver.pressTab(7);
        actionDriver.pressEnter(1);

        String backwindow = driver.getWindowHandle();

        for (String Child : driver.getWindowHandles()) {
            if (!backwindow.equalsIgnoreCase(Child)) {
                driver.switchTo().window(Child);
            }
        }

        /*List<String> windowlist = null;

        Set<String> windows = driver.getWindowHandles();

        windowlist = new ArrayList<String>(windows);

        String currentWindow = driver.getWindowHandle();
        System.out.println("Suburb... :" + currentWindow);

        if (!currentWindow.equalsIgnoreCase(windowlist.get(3 - 1)))
        {
            driver.switchTo().window(windowlist.get(3 - 1));
            System.out.println("Suburb...");
        }*/

    }

    public void selectSurburb(String suburb) throws InterruptedException, AWTException {

   /*     Thread.sleep(2000);

        String Parent_Window = driver.getWindowHandle();
        Set<String> AllWindows = driver.getWindowHandles();
        System.out.println("All Handles is : " + AllWindows.size());
        for (String Child : AllWindows) {
            if (!Parent_Window.equalsIgnoreCase(Child)) {
                driver.switchTo().window(Child);
            }
        }

        Thread.sleep(3000);
        driver.switchTo().window(mainWindow);
        System.out.println("Smallernyana Switched to Main Window : " + mainWindow );


*//*
        for (String winHandle : driver.getWindowHandles()) {

            if(!mainWindow.equals(winHandle))
            {
                System.out.println("Switched to Smallernyana Switched to Main Window : = " + winHandle);
                //driver.findElement(By.linkText("Back")).click();
                driver.switchTo().window(winHandle);
                break;
            }

        }
*//*
        Thread.sleep(3000);*/
    }

    public void selectProvince(String province) throws Exception {
        Thread.sleep(2000);
        actionDriver.pressTab(5);
        actionDriver.selectElementByVisibleText(drpProvince,province,"Province field");

    }

    public void clickSaveAndContinueButton() throws Exception {
        actionDriver.click(btnSaveAndContinue,"Save and Continue button");
    }

}
