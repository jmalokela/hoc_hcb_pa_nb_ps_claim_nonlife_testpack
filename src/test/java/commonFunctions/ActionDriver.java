package commonFunctions;

import com.github.javafaker.Faker;
import com.relevantcodes.extentreports.LogStatus;
import managers.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.ReporterConfig;
import org.testng.TestRunner;
import reporting.ExtentReportConfig;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ActionDriver extends ReporterConfig {

    public WebDriver driver;
    public Faker faker = new Faker();
    private String processName;
    public WebDriverManager webDriverManager;
    public ExtentReportConfig extentReportConfig;

    private static Logger log = Logger.getLogger(ActionDriver.class.getName());
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public ActionDriver() {
        //DOMConfigurator.configure("log4j.xml");
        webDriverManager = new WebDriverManager();
        this.driver = webDriverManager.getDriver();
    }

    /**
     * Taking a Screenshot
     * @param driver
     * @return
     */
    public static String screenShot(WebDriver driver) {

        String sDefaultPath = System.getProperty("user.dir");
        sDefaultPath = sDefaultPath.replace("batch", "");

        String date = dateFormat.format(new Date());
        //System.out.println("The date is " + date);
        String scrFolder = sDefaultPath + "\\ScreenShots\\"
                +"\\"+date+"\\";
        File newDir = new File(date);

        //deleteDir(newDir);

        if (!newDir.exists())
        {
            new File(scrFolder).mkdir();
        }

        System.setProperty("scr.folder", scrFolder);

        String screenTime = new SimpleDateFormat("HHmmss").format(
                Calendar.getInstance().getTime()).toString();

        String screenshotname = "screenshot";

        File scrFile = ((TakesScreenshot) driver)
                .getScreenshotAs(OutputType.FILE);

        //copy screenshot to screenshot folder
        try {
            FileUtils.copyFile(
                    scrFile,
                    new File(scrFolder
                            + "/"+screenshotname
                            +  screenTime
                            + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }


        return scrFolder + screenshotname + screenTime + ".png";
    }

    private static Runtime getRuntime() {
        return Runtime.getRuntime();
    }

    /**
     * Waiting for Element to be visible
     *
     * @param element
     * @param driver
     * @throws IOException
     */

    public static void waitForElement(WebElement element, WebDriver driver) throws IOException {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.visibilityOf(element));

        } catch (Exception e) {
            System.out.println("not visible");
            //ExtentReportConfig.childTest.log(LogStatus.FAIL,"Expected condition failed: waiting for visibility of element : " + element + ExtentReportConfig.childTest.addScreenCapture(ActionDriver.screenShot(driver)));

        }

    }
    /**
     * Used to launch application
     *
     * @param url -- application
     *            Example ---
     * @throws Exception
     */
    public void launchApplication(String url) throws IOException {
        try {
            log.trace("Entering application.");
            driver.get(url);
            driver.manage().window().maximize();

            //ExtentReportConfig.childTest.log(LogStatus.PASS,"Navigated to Application URL successfully : " + url + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.info("Navigated to Application URL successfully : " + url);

        } catch (Exception e) {
            ExtentReportConfig.childTest.log(LogStatus.FAIL,"Navigation unsuccessful : " + url + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.error("Navigation unsuccessful : " + url + " : " + e.getMessage());
            throw (e);
        }
    }

    /**
     * Clicking element function
     * @param elementName
     * @throws Exception
     */
    public void click(WebElement element, String elementName) throws Exception {
        try {

            element.click();
            ExtentReportConfig.childTest.log(LogStatus.PASS,"Able to perform click action on : " + elementName);// + elementName +  ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.info("Able to perform click action on : " + elementName);
        } catch (Exception e) {
            ExtentReportConfig.childTest.log(LogStatus.FAIL,"Unable to perform click action on : " + elementName +  ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.error("Unable to perform click action on : " + elementName + " : " + e.getMessage());
            throw (e);
        }

    }

    /**
     * Getting a page title and verifying a page title
     * @param expectedTitle
     * @throws IOException
     */
    public void verifyPageTitle(String expectedTitle,String testCaseName) throws IOException {
        try {
            Thread.sleep(2000);
            if(driver.getTitle().equals(expectedTitle))

                ExtentReportConfig.childTest.log(LogStatus.PASS,"Able to get the page title  : " + driver.getTitle());
            else
            {
                ExtentReportConfig.childTest.log(LogStatus.FAIL,"Unable to get the page title expected : " + expectedTitle + " but found : " + driver.getTitle() + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

                log.error("Unable to get the page title expected : " + expectedTitle + " but found : " + driver.getTitle());
            }

        }catch(Exception e)
        {
            e.getMessage();
        }
    }

    /**
     * perform enter value on a text field -- get from external file
     * @param element
     * @param testData
     * @param elementName
     * @throws Exception
     */
    public void enter(WebElement element, String testData, String elementName) throws Exception {
        try {
            waitForElement(element, driver);
            element.clear();
            element.sendKeys(testData);
            ExtentReportConfig.childTest.log(LogStatus.PASS,"Able to perform type in  : " + elementName + " Data is : " + testData );//+ ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.info("Able to perform type in  : " + elementName + " Data is : " + testData);
        } catch (Exception e) {
            ExtentReportConfig.childTest.log(LogStatus.FAIL,"Unable to perform type in : " + elementName + " Data is : " + testData + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.error("Unable to perform type in : " + elementName + " : " + e.getMessage());
            throw (e);
        }

    }

    /**
     * perform mouse hover on given element
     *
     * @param element
     * @throws Exception
     */
    public void mouseHover(WebElement element, String elementName,String testCaseName) throws Exception {
        try {
            Actions a = new Actions(driver);
            waitForElement(element, driver);
            WebElement mo = element;
            a.moveToElement(mo).build().perform();
            ExtentReportConfig.childTest.log(LogStatus.PASS,"Able to perform mouseHover on : " + elementName + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.info("Able to perform mouseHover on : " + elementName);

        } catch (Exception e) {
            ExtentReportConfig.childTest.log(LogStatus.FAIL,"Unable to perform mouseHover on : " + elementName + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.error("Unable to perform mouseHover on : " + elementName + " : " + e.getMessage());
            throw (e);
        }

    }

    /**
     * Selecting Element by visible text
     *
     * @param element
     * @param testData
     * @param elementName
     * @throws Exception
     */

    public void selectElementByVisibleText(WebElement element, String testData, String elementName) throws Exception {
        try {
            Select selectCity = new Select(element);
            selectCity.selectByVisibleText(testData);
            ExtentReportConfig.childTest.log(LogStatus.PASS,"Able to perform select in  : " + elementName + " Data is : " + testData);// +  ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.info("Able to perform select in  : " + elementName + " Data is : " + testData + "");

        } catch (Exception e) {
            ExtentReportConfig.childTest.log(LogStatus.FAIL,"Unable to perform select on : " + elementName + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.error("Unable to perform select on : " + elementName + " : " + e.getMessage());
            throw (e);
        }


    }

    /**
     * Getting Element text
     *
     * @param locator
     * @param messsage
     * @param elementName
     * @throws Exception
     */
    public void getText(By locator, String messsage, String elementName,String testCaseName) throws Exception {
        try {
            WebElement msg = driver.findElement(locator);
            String elemMesssage = msg.getText();
            ExtentReportConfig.childTest.log(LogStatus.PASS,"Able to get the message from : " + elementName + " Message : " + elemMesssage);// + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            //Log.info("Able to get the message from : " + elementName + " Message : " + elemMesssage + " ");

            if (!elemMesssage.equals(messsage)) {
                ExtentReportConfig.childTest.log(LogStatus.FAIL,"The message found is not expected : " + elementName + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

                //Log.error("The message found is not expected : " + elementName);
                //throw new Exception("Failed with massage : " + msg.getText());
            }


        } catch (Exception e) {
            ExtentReportConfig.childTest.log(LogStatus.FAIL,"Unable to get the text from element: " + elementName + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            //Log.error("Unable to get the text from element: " + elementName + " : " + e.getMessage());
            throw (e);
        }

    }

    /**
     * Getting Element Value
     *
     * @param locator
     * @param elementName
     * @return
     * @throws Exception
     */
    public String getValue(By locator, String elementName,String testCaseName) throws Exception {
        String value = "";
        try {
            value = driver.findElement(locator).getText();
            ExtentReportConfig.childTest.log(LogStatus.FAIL,"Able to get the : " + elementName + "- Value is : " + value);// + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.info("Able to get the : " + elementName + "- Value is : " + value + " ");

        } catch (Exception e) {
            ExtentReportConfig.childTest.log(LogStatus.PASS,"Unable to get the : " + elementName +  ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.error("Unable to get the : " + elementName + " : " + e.getMessage());
            throw (e);
        }

        return value;
    }

    /**
     * Click Element displayed by list
     *
     * @param locator
     * @param elementName
     * @throws Exception
     */
    public void listEmelentsClick(By locator, String elementName,String testCaseName) throws Exception {
        try {
            List<WebElement> click = driver.findElements(locator);
            for (WebElement elem : click) {
                elem.click();
                ExtentReportConfig.childTest.log(LogStatus.PASS,"Able to perform select on : " + elementName);// +  ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

                log.info("Able to perform select on : " + elementName);
                break;
            }

        } catch (Exception e) {
            ExtentReportConfig.childTest.log(LogStatus.FAIL,"Unable to perform select on : " + elementName + ExtentReportConfig.childTest.addScreenCapture(screenShot(driver)));

            log.error("Unable to perform select on : " + elementName + " : " + e.getMessage());
            throw (e);
        }
    }

    /**
     * Getting text field value
     * @param locator
     * @param elementName
     * @return
     * @throws Exception
     */
    public String checkFieldValue(By locator, String elementName) throws Exception {
        String value = "";
        value = driver.findElement(locator).getAttribute("value");
        if (!value.equals("")) {
            ExtentReportConfig.childTest.log(LogStatus.PASS,"Able to get value on : " + elementName + " value is : " + value );
        }
        Reporter.log(value, true);
        return value;
    }

    public void WindowsProcess(String processName) {
        this.processName = processName;
    }

    public void CloseRunningProcess() throws Exception {
        if (isRunning()) {
            getRuntime().exec("taskkill /F /IM " + processName);
            System.out.println("Process Closed");
        }
    }

    private boolean isRunning() throws Exception {
        Process listTasksProcess = getRuntime().exec("tasklist");
        BufferedReader tasksListReader = new BufferedReader(
                new InputStreamReader(listTasksProcess.getInputStream()));

        String tasksLine;

        while ((tasksLine = tasksListReader.readLine()) != null) {
            if (tasksLine.contains(processName)) {
                return true;
            }
        }

        return false;
    }

    public void pressA() throws AWTException, InterruptedException {

        Robot r = new Robot();
        r.keyPress(KeyEvent.VK_A);
        r.keyRelease(KeyEvent.VK_A);
        Thread.sleep(1000);
    }

    /**
     * perform page refresh
     * @throws InterruptedException
     * @throws AWTException
     */
    public void refreshPage() throws InterruptedException, AWTException {
        Thread.sleep(3000);
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_F5);
        robot.keyRelease(KeyEvent.VK_F5);
    }
    public void pressTab(int numberOfTimes) throws AWTException {
        for(int i=1;i<=numberOfTimes;i++){
            Robot r = new Robot();
            try
            {
                r.keyPress(KeyEvent.VK_TAB);
                r.keyRelease(KeyEvent.VK_TAB);
                Thread.sleep(1000);
                System.out.println("Tabbed "+i+" times.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void pressEnter(int numberOfTimes)
    {
        for(int i=1;i<=numberOfTimes;i++){
            Robot r = null;
            try
            {
                r = new Robot();
                r.keyPress(KeyEvent.VK_ENTER);
                r.keyRelease(KeyEvent.VK_ENTER);
            } catch (AWTException e) {
                e.printStackTrace();
            }

        }
    }


    /**
     * Generating Random Full Name
     */
    public String randomFullName() {
        return faker.name().fullName();
    }

    /**
     * Generating Random Name
     */
    public String randomFirstName() {
        return faker.name().firstName();
    }

    /**
     * Generating Random Surname
     */
    public String randomLastName() {
        return faker.name().lastName();
    }

    public String randomMobile() {
        return faker.number().digits(8);
    }

    /**
     * Killing a process by name
     */
    public void killProcess(String processName) {
        try {
            Runtime.getRuntime().exec("taskkill /F /IM " + processName);
            Reporter.log("Process Killed");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //Retry clicking on the element
    public boolean retryingFindClick(WebElement element,String testCaseName) {
        boolean result = false;
        int attempts = 0;
        while (attempts < 5) {
            try {
                element.click();
                ExtentReportConfig.childTest.log(LogStatus.PASS,"Able to perform click on : " + element.getText() +  ExtentReportConfig.childTest.addScreenCapture(this.screenShot(driver)));
                result = true;
                break;
            } catch (Exception e) {

            }
            attempts++;
        }
        return result;
    }

    /**
     * Check if Element Exist
     * @param element
     * @return
     */

    public boolean checkIfObjectExists(WebElement element) {
        boolean exists = false;
        try {
            exists = (element != null) || (element.isEnabled() == true);
            Reporter.log("Object exists" + element.toString(), true);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            exists = false;
        }
        return exists;


    }

    /**
     * Getting current date and time
     * @return
     */
    public String getDateAndTime()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String date = dateFormat.format(new Date());
        return date;
    }

    /**
     * Getting test case name
     * @param sTestCase
     * @return
     * @throws Exception
     */
    public String getTestCaseName(String sTestCase) throws Exception {
        String value = sTestCase;
        try {
            //int posi = value.indexOf("@");
            int posi = value.lastIndexOf(".");
            value = value.substring(posi + 1);
            return value;
        } catch (Exception e) {
            log.error("Class Utils | Method getTestCaseName | Exception desc : " + e.getMessage());
            throw (e);
        }
    }

    /**
     * Scrolling to the end of the page
     */
    public void scrollByEndOfPage()
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

}
