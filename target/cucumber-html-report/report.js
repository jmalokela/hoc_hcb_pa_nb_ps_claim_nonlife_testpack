$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Regression_TS_NB_02_TC009.feature");
formatter.feature({
  "line": 1,
  "name": "Validate the policy issuance of HOC and payment cycle generation",
  "description": "",
  "id": "validate-the-policy-issuance-of-hoc-and-payment-cycle-generation",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "To validate the policy issuance of HOC and payment cycle generation",
  "description": "",
  "id": "validate-the-policy-issuance-of-hoc-and-payment-cycle-generation;to-validate-the-policy-issuance-of-hoc-and-payment-cycle-generation",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 2,
      "name": "@Regression_TS_NB_02_TC009"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "user is logged in",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "click on business operations",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "click on new business tab",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "click on Application Data Capture",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "select LOB and product as homeowner cover",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "click on proceed button",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "enter expiry date",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "search for party holder by  opening party search form",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "enter the Party Code",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "click on Search button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "verify that the party holder details are returned",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "select the party holder by clicking on the party code",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "click on Save button and continue",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "verify account details and click on continue",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "select policy type",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "enter bond start date",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "enter bond number",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "select payment mode option",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "click on save and continue button",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "verify that the HOC Risk screen is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "click on Add button",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "enter sum insured amount",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "enter address house number",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "enter address street name",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "enter suburb",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "select the displayed suburb",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "enter the province",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "click on Save and Continue",
  "keyword": "And "
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.user_is_logged_in()"
});
formatter.result({
  "duration": 14982708400,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.click_on_business_operations()"
});
formatter.result({
  "duration": 4265631700,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.click_on_new_business_tab()"
});
formatter.result({
  "duration": 2267575100,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.click_on_Application_Data_Capture()"
});
formatter.result({
  "duration": 2056189400,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.select_LOB_and_product_as_homeowner_cover()"
});
formatter.result({
  "duration": 1089157200,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.click_on_proceed_button()"
});
formatter.result({
  "duration": 3982548700,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.enter_expiry_date()"
});
formatter.result({
  "duration": 808516600,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.search_for_party_holder_by_opening_party_search_form()"
});
formatter.result({
  "duration": 1643559700,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.enter_the_Party_Code()"
});
formatter.result({
  "duration": 3462444900,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.click_on_Search_button()"
});
formatter.result({
  "duration": 2915183300,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.verify_that_the_party_holder_details_are_returned()"
});
formatter.result({
  "duration": 286431800,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.select_the_party_holder_by_clicking_on_the_party_code()"
});
formatter.result({
  "duration": 2240803200,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.click_on_Save_button()"
});
formatter.result({
  "duration": 20931582900,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.verify_account_details_and_click_on_continue()"
});
formatter.result({
  "duration": 6149768200,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.select_policy_type()"
});
formatter.result({
  "duration": 2553389000,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.enter_bond_start_date()"
});
formatter.result({
  "duration": 4066295900,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.enter_bond_number()"
});
formatter.result({
  "duration": 1475486600,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.select_payment_mode_option()"
});
formatter.result({
  "duration": 948829000,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.click_on_save_and_continue_button()"
});
formatter.result({
  "duration": 6480150700,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.verifyThatTheHOCRiskScreenIsDisplayed()"
});
formatter.result({
  "duration": 118429900,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.clickOnAddButton()"
});
formatter.result({
  "duration": 3788246000,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.enterSumInsuredAmount()"
});
formatter.result({
  "duration": 2078520200,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.enterAddressHouseNumber()"
});
formatter.result({
  "duration": 2077601700,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.enterAddressStreetName()"
});
formatter.result({
  "duration": 2163703800,
  "status": "passed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.enterSuburb()"
});
formatter.result({
  "duration": 312686450000,
  "error_message": "org.openqa.selenium.TimeoutException: Timed out waiting for page to load.\nBuild info: version: \u00273.7.0\u0027, revision: \u00272321c73\u0027, time: \u00272017-11-02T22:22:35.584Z\u0027\nSystem info: host: \u0027L0610062247\u0027, ip: \u0027172.28.1.60\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_231\u0027\nDriver info: org.openqa.selenium.ie.InternetExplorerDriver\nCapabilities {acceptInsecureCerts: false, browserName: internet explorer, browserVersion: 11, javascriptEnabled: true, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), se:ieOptions: {browserAttachTimeout: 0, elementScrollBehavior: 0, enablePersistentHover: true, ie.browserCommandLineSwitches: , ie.ensureCleanSession: false, ie.fileUploadDialogTimeout: 3000, ie.forceCreateProcessApi: false, ignoreProtectedModeSettings: false, ignoreZoomSetting: false, initialBrowserUrl: http://localhost:2299/, nativeEvents: true, requireWindowFocus: false}, setWindowRect: true, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}}\nSession ID: 2cff8764-8c15-44a6-9c8e-d22d41d1aaa1\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:600)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.window(RemoteWebDriver.java:957)\r\n\tat pageobjects.ApplicationDataCapturePage.enterSuburb(ApplicationDataCapturePage.java:238)\r\n\tat teststeps.Regression_TS_NB_02_TC009Steps.enterSuburb(Regression_TS_NB_02_TC009Steps.java:173)\r\n\tat ✽.And enter suburb(Regression_TS_NB_02_TC009.feature:28)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.selectTheDisplayedSuburb()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.enterTheProvince()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Regression_TS_NB_02_TC009Steps.clickOnSaveAndContinue()"
});
formatter.result({
  "status": "skipped"
});
});