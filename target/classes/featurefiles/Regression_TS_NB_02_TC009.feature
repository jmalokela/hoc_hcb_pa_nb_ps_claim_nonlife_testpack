Feature: Validate the policy issuance of HOC and payment cycle generation
  @Regression_TS_NB_02_TC009
  Scenario: To validate the policy issuance of HOC and payment cycle generation
    Given user is logged in
    Then click on business operations
    And click on new business tab
    Then click on Application Data Capture
    Then select LOB and product as homeowner cover
    Then click on proceed button
    Then enter expiry date
    Then search for party holder by  opening party search form
    Then enter the Party Code
    And click on Search button
    And verify that the party holder details are returned
    Then select the party holder by clicking on the party code
    And click on Save button and continue
    And verify account details and click on continue
    And select policy type
    And enter bond start date
    And enter bond number
    And select payment mode option
    And click on save and continue button
    Then verify that the HOC Risk screen is displayed
    And click on Add button
    And enter sum insured amount
    And enter address house number
    And enter address street name
    And enter suburb
    And select the displayed suburb
    And enter the province
    And click on Save and Continue





