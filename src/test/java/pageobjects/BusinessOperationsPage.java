package pageobjects;

import commonFunctions.ActionDriver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BusinessOperationsPage {

    public WebDriver driver;
    ActionDriver actionDriver;
    private String mainWindow;

    public BusinessOperationsPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
        actionDriver = new ActionDriver();

    }

    @FindBy(id="pMenu_2")
    private WebElement linkNewBusiness;

    @FindBy(xpath="//*[contains(@onclick,'gotoNextScreenUSA()')]")
    private WebElement linkApplicationDataCapture;

    @FindBy(name="pLOB1")
    private WebElement drpLOB;

    @FindBy(name="pProduct")
    private WebElement drpProduct;

    @FindBy(name="Proceed")
    private WebElement btnProceed;

    @FindBy(id="SEC-BSCDTLExpiry Date")
    private WebElement txtExpiryDate;

    @FindBy(xpath="//img[@src='http://sahara-preapp1:9080/IIMS/target/source/images/LookUp.bmp']")
    private WebElement btnPartyHolderLookup;

    @FindBy(name="pPartyCode")
    private WebElement txtPartyCode;

    @FindBy(linkText="Search")
    private WebElement btnSearch;
    //Save button
    @FindBy(id="saveBtn")
    private WebElement btnSave;
    //Continue button
    @FindBy(id="saveConBtn")
    private WebElement btnContinue;

    @FindBy(name="COND-T1Policy Type")
    private WebElement drpPolicyType;

    public void clickNewBusiness() throws Exception {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(driver.findElement(By.xpath("//frame[@name='head']")));
        actionDriver.click(linkNewBusiness,"New Business link");
        mainWindow = driver.getWindowHandle();
    }

    public void clickApplicationDataCapture()
    {
        linkApplicationDataCapture.click();
    }

    public void selectLOB(String LOB) throws Exception {
        String lob = LOB.trim();
        driver.switchTo().defaultContent();
        driver.switchTo().frame(driver.findElement(By.xpath("//*[@name='display']")));
        actionDriver.selectElementByVisibleText(drpLOB,lob,"LOB dropdown");

    }

    public void selectProduct(String product) throws Exception {
        actionDriver.selectElementByVisibleText(drpProduct,product,"LOB dropdown");
    }

    public void clickProceed() throws Exception {

        actionDriver.click(btnProceed,"Proceed Button");
    }

    public void enterExpiryDate(String expDate) throws Exception {
        actionDriver.enter(txtExpiryDate,expDate,"Expiry Date field");
    }

    public void clickPartyHolderLookup() throws  Exception
    {
        actionDriver.click(btnPartyHolderLookup,"Party Holder Lookup button");
    }

    public void enterPartyCode(String partyCode) throws Exception {
        Thread.sleep(2000);
        driver.switchTo().defaultContent();
        //String mainWindow = driver.getWindowHandle();
        System.out.println("Window handling "+mainWindow);

        System.out.println("Handling has done" );
        for (String winHandle : driver.getWindowHandles()) {

            if(!mainWindow.equals(winHandle))
            {
                System.out.printf(winHandle);
                driver.switchTo().window(winHandle);
                break;
            }

        }
        actionDriver.enter(txtPartyCode,partyCode,"Party Code field");
    }

    public void selectPartyHolder(String partyCode) throws Exception {
        actionDriver.click(driver.findElement(By.linkText(partyCode)),"Party Holder");
    }

    public void clickSearchButton() throws Exception {
        actionDriver.click(btnSearch,"Search Button");
    }

    public void verifyPartyHolderDetailsReturned()
    {
        Assert.assertTrue(driver.findElement(By.id("firstFocusNew1")).isDisplayed());
    }

    public void clickSaveButton() throws Exception {
        driver.switchTo().window(mainWindow);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(driver.findElement(By.xpath("//*[@name='display']")));
        actionDriver.click(btnSave,"Save Button");
        actionDriver.click(btnSave,"Save Button");
        actionDriver.click(btnContinue,"Continue button");

    }

    public void clickContinueButton() throws Exception {
        actionDriver.click(driver.findElement(By.className("form_field-txt1")),"Check box");
        actionDriver.pressTab(5);
        actionDriver.pressEnter(1);
    }

    public void selectPolicyType() throws Exception {
        actionDriver.selectElementByVisibleText(drpPolicyType,"Internal","Policy Type");
    }

    public void clickSaveAndContinueButton() throws Exception {
        actionDriver.click(btnSave,"Save Button");
        actionDriver.click(btnContinue,"Continue button");
    }

}
