Feature: TCS Bancs Pre-Prod Login
  @BancsLogin
  Scenario: Login to Bancs with valid credentials
    Given User want to login to Bancs by navigating to Home page
    Then User enter username and Password
    And enter branch
    And enter role
    Then clicks on Login button
    Then verify that the landing page is displayed
    Then logout
