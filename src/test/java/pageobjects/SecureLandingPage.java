package pageobjects;

import commonFunctions.ActionDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SecureLandingPage {

    private WebDriverWait webDriverWait;
    private WebDriver driver;
    private ActionDriver actionDriver;

    //Business Operation link
    @FindBy(xpath="//img[@src='../images/Business-icon.png']")
    private WebElement linkBusinessOperations;
    //Dash Board link
    @FindBy(xpath="//img[@src='../images/Dashboard-icon.png']")
    private WebElement dashboard;

    @FindBy( xpath="//img[@src='../images/icon_logout.png']")
    private WebElement btnLogout;


    public SecureLandingPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
        actionDriver = new ActionDriver();
    }

    public void clickOperations() throws Exception {
        actionDriver.click(linkBusinessOperations,"Business Operations link");
    }

    public void clickLogoutButton() throws Exception {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(driver.findElement(By.xpath("//frame[@name='head']")));
        actionDriver.click(btnLogout,"Logout button");
    }

}
