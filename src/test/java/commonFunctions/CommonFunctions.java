package commonFunctions;

import com.relevantcodes.extentreports.LogStatus;
import context.TestContext;
import pageobjects.HomePage;
import runner.TestRunner;

public class CommonFunctions {

    private HomePage homePage;
    public ActionDriver actionDriver;
    TestContext testContext;

    public CommonFunctions(TestContext context)
    {
        homePage = context.getPageObjectManager().getHomePage();
    }

    public void Login(String username, String password,String branch,String role) throws Exception {
        actionDriver = new ActionDriver();

        homePage.navigateToHomePage();
        homePage.switchToDisplayFrame();
        homePage.enterUsername(username);
        homePage.enterPassword(password);
        homePage.enterBranch(branch);
        homePage.enterRole(role);
        homePage.clickLoginButton();

    }
}
