
package runner;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import managers.WebDriverManager;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.testng.ReporterConfig;
import reporting.ExtentReportConfig;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/main/resources/featurefiles",
				tags={"@Regression_TS_NB_02_TC009"},
				glue= {"teststeps"},
				dryRun = false,
				monochrome = true,
				format={"pretty","html:target/cucumber-html-report"},
				plugin = {"json:target/cucumber.json", "junit:target/cucumber.xml", "rerun:target/rerun.txt"}
		)
public class TestRunner {

	@BeforeClass
	public static void beforeMethod() throws UnknownHostException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
		System.setProperty("currentdate",dateFormat.format(new Date()));
		ExtentReportConfig.createReport();
		System.out.println("Print Print");
	}

	@AfterClass
	public static void afterMethod() throws Exception {
		if(WebDriverManager.driverType != null)
		{
			Thread.sleep(2000);
			//WebDriverManager.closeDriver();
			ExtentReportConfig.flushReport();
			System.out.println("Total Memory used : " + Runtime.getRuntime().maxMemory());
			//Runtime.getRuntime().exec("RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 255");
			WebDriverManager.ieKiller();
			Thread.sleep(2000);
		}
	}
}