package pageobjects;

import commonFunctions.ActionDriver;
import managers.FileReaderManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class HomePage{

    public WebDriverWait webDriverWait;
    public WebDriver driver;
    public ActionDriver actionDriver;

    @FindBy(xpath = "//frame[@name='display']")
    private WebElement frameDisplay;
    //Username field
    @FindBy(name="pUserName")
    private WebElement txtUsername;
    //Password field
    @FindBy(name="pPassword")
    private WebElement txtPassword;
    //Select button
    @FindBy(name="pSelectBranch")
    private WebElement branch;
    //Role textfield
    @FindBy(name="pRole")
    private WebElement txtRole;


    @FindBy(xpath = "//td[@class=\"form_field_txt1\"]//*[@id=\"textfield4\"]")
    private WebElement drpRole;

    @FindBy(linkText = "Login")
    private WebElement btnLogin;

    //Preprod Properties

    public HomePage(WebDriver driver)
    {
        this.driver  = driver;
        PageFactory.initElements(driver,this);
        actionDriver = new ActionDriver();
    }

    public void enterUsername(String username) throws Exception {
        actionDriver.enter(txtUsername,username,"Username field");
    }

    public void enterPassword(String passwd) throws Exception {
        actionDriver.enter(txtPassword,passwd,"Password field");

    }

    public void enterBranch(String branchc) throws Exception {
        actionDriver.enter(branch,branchc,"Branch field");

    }

    public void enterRole(String role) throws Exception {
        actionDriver.enter(txtRole,role,"Role field");
    }

    public void clickLoginButton() throws Exception {
        actionDriver.click(btnLogin,"Login button");
    }

    public void switchToDisplayFrame()
    {
        if(driver.findElements(By.xpath("//frame[@name='display']")).size()>0)
        {
            driver.switchTo().frame(driver.findElement(By.xpath("//frame[@name='display']")));
        }
    }

    public void selectRole(String role)
    {
        Select select = new Select(driver.findElement(By.xpath("//td[@class=\"form_field_txt1\"]//*[@id=\"textfield4\"]")));
        select.selectByValue(role);
    }

    public void navigateToHomePage() throws IOException {
        actionDriver.launchApplication(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());

    }

}
