package teststeps;

import com.relevantcodes.extentreports.LogStatus;
import context.TestContext;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import pageobjects.BusinessOperationsPage;
import pageobjects.HomePage;
import pageobjects.SecureLandingPage;
import reporting.ExtentReportConfig;
import runner.TestRunner;

import java.io.IOException;

/**
 * Author : Jan Malkela
 * Date : 2020-02-11
 */

public class LoginSteps {

    private HomePage homePage;
    private TestContext testContext;
    private SecureLandingPage secureLandingPage;

    public LoginSteps(TestContext context)
    {
        this.testContext = context;
        homePage = context.getPageObjectManager().getHomePage();
        secureLandingPage = context.getPageObjectManager().getSecureLandingPage();
        ExtentReportConfig.childTest = ExtentReportConfig.extent.startTest("Login");

    }

    @Given("^User want to login to Bancs by navigating to Home page$")
    public void user_want_to_login_to_Bancs_by_navigating_to_Home_page() throws IOException {
        homePage.navigateToHomePage();
        homePage.switchToDisplayFrame();

    }

    @Then("^User enter username and Password$")
    public void user_enter_username_and_Password() throws Exception {
        homePage.enterUsername("NB309774");
        homePage.enterPassword("kolkata@1");

    }

    @Then("^enter branch$")
    public void enter_branch() throws Exception {
        homePage.enterBranch("HO");

    }

    @Then("^enter role$")
    public void enter_role() throws Exception {

        homePage.enterRole("IT-HELP");
    }

    @Then("^clicks on Login button$")
    public void clicks_on_Login_button() throws Exception {
        homePage.clickLoginButton();

    }

    @Then("^verify that the landing page is displayed$")
    public void verify_that_the_landing_page_is_displayed(){
       // Assert.assertTrue();
    }

    @Then("^logout$")
    public void logout() throws Exception {
        secureLandingPage.clickLogoutButton();
    }

}
