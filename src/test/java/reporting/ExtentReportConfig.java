package reporting;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import runner.TestRunner;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExtentReportConfig {

    public static ExtentReports extent;
    public static ExtentTest parentTest;
    public static ExtentTest childTest;
    private static String sDefaultPath = System.getProperty("user.dir");;

    public static void createReport() throws UnknownHostException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
        InetAddress inetAddress = InetAddress.getLocalHost();
        sDefaultPath = sDefaultPath.replace("batch", "");
        String date = dateFormat.format(new Date());
        System.out.println(date);
        extent = new ExtentReports(sDefaultPath + "\\SummaryReports\\SummaryReport_"+date+".html");
        File extentXml = new File(System.getProperty("user.dir")+"\\extent-config.xml");
        extent.loadConfig(extentXml);

    }

    public static void flushReport()
    {
        extent.flush();
    }
}
