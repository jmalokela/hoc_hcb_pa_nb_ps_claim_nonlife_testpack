package context;
 
import managers.PageObjectManager;
import managers.WebDriverManager;
import pageobjects.ApplicationDataCapturePage;

public class TestContext {
	private WebDriverManager webDriverManager;
	private PageObjectManager pageObjectManager;
	private ApplicationDataCapturePage applicationDataCapturePage;
	
	public TestContext()
	{
		webDriverManager = new WebDriverManager();
		pageObjectManager = new PageObjectManager(webDriverManager.getDriver());
		applicationDataCapturePage = new ApplicationDataCapturePage(webDriverManager.getDriver());
	}
	
	public WebDriverManager getWebDriverManager()
	{
		return webDriverManager;
	}
	
	public PageObjectManager getPageObjectManager()
	{
		return pageObjectManager;
	}

	public ApplicationDataCapturePage getApplicationDateCapturePage()
	{
		return applicationDataCapturePage;
	}
 
}